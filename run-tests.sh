#!/usr/bin/env sh

set -v
set -e

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f ${CONTAINER_NAME} 2>&1 > /dev/null || true

docker run --name ${CONTAINER_NAME} \
 --privileged \
 --device /dev/fuse \
 -e START_KEYBASE="true" \
 -e KEYBASE_USERNAME="${KEYBASE_USERNAME}" \
 -e KEYBASE_PAPERKEY="${KEYBASE_PAPERKEY}" \
 -v /var/run/docker.sock:/var/run/docker.sock \
 -dt ${CONTAINER_TEST_IMAGE}

sleep 30

docker exec -t ${CONTAINER_NAME} ash -c 'timeout 1 ls'
docker exec -t ${CONTAINER_NAME} ash -c 'cat /keybase/private/builduser/test.txt'

# clean up
docker rm -f ${CONTAINER_NAME}
